import express from "express";
import AWS from "aws-sdk";

import thumbor from "./services/thumbor";

const app = express();

const credentials = new AWS.SharedIniFileCredentials({
  profile: "thumbor-s3-images-optimization"
});

AWS.config.credentials = credentials;
AWS.config.getCredentials(err => {
  if (err) console.log(err);
});

const s3 = new AWS.S3();

const getObjectsKeys = (params, out = []) =>
  new Promise((resolve, reject) => {
    s3.listObjectsV2(params)
      .promise()
      .then(({ Contents, IsTruncated, NextContinuationToken }) => {
        Contents.map(object => {
          if (object.Size > 0) {
            out.push(object.Key);
          }
        });

        !IsTruncated
          ? resolve(out)
          : resolve(
              getObjectsKeys(
                Object.assign(params, {
                  ContinuationToken: NextContinuationToken
                }),
                out
              )
            );
      })
      .catch(reject);
  });

function compressImages(keys) {
  var acceptable_extensions = ["jpg", "jpeg", "png"];
  keys.map(async key => {
    var key_extension = key.split(".");
    key_extension = key_extension[key_extension.length - 1];

    if (acceptable_extensions.includes(key_extension)) {
      if (key_extension === "png") {
        var builded_url =
          "/unsafe/0x0/filters:format(jpeg):quality(80)/https://images-optimization.s3-sa-east-1.amazonaws.com/" +
          key;
      } else {
        var builded_url =
          "/unsafe/0x0/filters:quality(80)/https://images-optimization.s3-sa-east-1.amazonaws.com/" +
          key;
      }

      try {
        await thumbor
          .get(builded_url.replace(/ /g, "+"), {
            responseType: "arraybuffer"
          })
          .then(response => {
            var put_params = {
              Body: response.data,
              Bucket: "images-optimization",
              Key: key,
              // Key: key.replace(".png", ".jpeg"),
              ACL: "public-read",
              ContentType: "image/jpeg"
            };

            s3.putObject(put_params, function(err, data) {
              if (err) {
                console.log(err, err.stack);
                return false;
              }
              return true;
            });
          });
      } catch (err) {
        console.log(err);
      }
    }
  });
}

let params = { Bucket: "images-optimization" };
getObjectsKeys(params)
  .then(async response => {
    await compressImages(response);
  })
  .catch(console.log);

app.listen(3333);
