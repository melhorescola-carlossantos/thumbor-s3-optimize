import axios from "axios";

const thumbor = axios.create({
  baseURL: "http://localhost:8000"
});

export default thumbor;
