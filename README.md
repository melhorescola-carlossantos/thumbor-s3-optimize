# Optimize images on s3 with Thumbor.
This little project do not worry about the treatment of the errors and don't have so much options to configure the resources of aws-sdk or the thumbor.org.

## Minimal usage
Create the shared credentials file in your OS.

> Linux, Unix, and MacOS: ~/.aws/credentials
> Windows: C:\User\USER_NAME\.aws.credentials

Your credentials file must be something like this:
```
[default]
aws_access_key_id = <YOUR_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>

[s3-staging]
aws_access_key_id = <YOUR_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>

[s3-production]
aws_access_key_id = <YOUR_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>
```

#### Find the configuration of your s3 and put your credential alias

```js
const credentials = new AWS.SharedIniFileCredentials({
  profile: "YOUR_CREDENTIAL_ALIAS_HERE"
});
```

Running the thumbor server with Docker
```docker
$ docker run -p 8000:8000 apsl/thumbor
```

For more information about apsl/thumbor access: [APSL / Thumbor](https://github.com/APSL/docker-thumbor)

You don't need to have an experience with python or another programming language but it is satisfactory to have some basic knows about docker and GET Request.


